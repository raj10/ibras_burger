<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Sobre Nostros</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
</head>
<body id="wrapper" class="rest">
	<div id ="modal" class="modal-register-gradeout">
		
		<div id ="modal-register">
			<span id = "closebtn" class="closebtn">&times;</span>
			<div id ="register-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Registro de Usario
			</div>
			<br><br>
			<hr>


			<form action="register" method="post" id="registration-form">
				{{ csrf_field() }}

				<p style="color: red;"> 

				@error('username') {{ $message }} @enderror 
				 @error('email') {{ $message }} @enderror
				 @error('email') {{ $message }} @enderror 
				 @error('password') {{ $message }} @enderror
				 @error('repeatpass') {{ $message }} @enderror
				  @error('address') {{ $message }} @enderror


				</p>

				<label for="fullname">Nombre y apellido:</label>
				<input type="text" name="username" id=fullname required
				    title="Username must have only alphabets and numbers."
				    pattern="^[a-zA-Z0-9]*$"
				    >

				<label for="mail">Correo:</label>
				<input type="email" name="email" id="mail" required
				    title="Example email: youremail@gmail.com"
				    pattern="[a-z0-9._%+-]+@gmail.com">
 

				<label for="pass">Contrasena:</label>
				<input type="password" name="password" id="pass" required
				    title="Password must contain at least 8 characters upto 10 characters, including atleast one uppercase, lowercase, number and special character." 
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >

				<label for="repeatPass">Repetir Contrasena:</label>
				<input type="password" name="repeatpass" id="repeatPass" required
				    title="Please enter the same Password as before."
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >
				<label for="address">Direccion:</label>
				<textarea name="address" id = "addresss" required></textarea>

				<input type="submit" name="submitregistration" value="Cargar" id  ="sendBtn">

			</form>
			
		</div>
	</div>

	<div id ="modal1" class="modal-login-gradeout">
		<div id ="modal-login">
			<span id = "closebtn1" class="closebtn">&times;</span>
			<div id ="login-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Iniciar Session
			</div>
			<br><br>
			<hr>


			<form action="login" method="post" id="login-form">
				{{ csrf_field() }}

				<p style="color: red;">
					@error('username') {{ $message }} @enderror
					@error('password') {{ $message }} @enderror 


				</p>

				<label for="userName">Usuario:</label>
				<input type="text" name="username" id=userName required>

				<label for="pass1">Contrasena:</label>
				<input type="password" name="password" id="pass1" required >

				<a href="#"><input type="submit" value="Entrar" id  ="enterBtn"></a>
				@if(Session::has('message'))
              	<p>
        	    	{{ Session::get('message') }}
               	</p>
           		@endif 
			</form>
		</div>
	</div>


	<header class="rest">
		<div id = "header-gradeout">
			<img src="images/5.png" class="logo" width="100px" align="center" />
				@if (Session::has('user'))
				{
				<a  href="index.php">INICIO</a>
				<a class="active" href="sobrenostros">SOBRE NOSTROS</a>
				<a href="menu">MENU</a>
				<a href="blog/">BLOG</a>
				<a  href="contacto">CONTACTO</a>
				<a  href="editarperfil">EDITAR PERFIL</a>
				<a  href="logout">CERRAR SESION</a> 
				}
				@else{
					<a href="index.php">INICIO</a>
					<a class="active" href="sobrenostros">SOBRE NOSTROS</a>
					<a href="menu">MENU</a>
					<a href="blog/">BLOG</a>
					<a  href="contacto">CONTACTO</a>
					<a id ="registerBtn" >REGISTRO</a>
					<a id ="loginBtn" >INICIAR SESION</a>
				}
			    @endif
			
		</div>
		<div class="nostrosbanner">
			
		<div id ="banner-gradeout">
				<div class ="banner-text">
					<h3 class="intro">LAS MEJORES DE LA CIUDAD</h3>
					<h2 class='intro'>Sobre nuestras Hamburguesas</h2>
				</div>
			</div>
		</div>	

	</header>

	<div class="nostrosbody">
			<!-- <br><br><br><br><br><br><br><br><br><br><br><br>
			<br> -->
			<div class="nostrosimgs" style="width:100%">
				<div class="burg1" style="float: left; width: 50%;"><img src="images/hamburguesa1.jpg" alt="mixta" width="350px" height="300px"></div>
				<div class="burg2"><img src="<?php echo asset('images/hamburguesa2.jpg')?>" alt="mixta" width="350px" height="300px"></div>
			</div>
 
				
			<div>
			<h2 class="intro">Somos Ibra</h2>
		<p id="nostrospara">
			Al comenzar el dia, cada mañana estamos más que preparándonos para dar lo mejor de nosotros mismos.<br>
			Porque cada uno de nuestros clientes nos inspira a trabajar en pro del mejor servicio, las mejores entregas y, sobre todo, las<br>
			mejores Hamburguesas..
		</p>
		<p id="para">
			Los orígenes se remontan al 10 de junio de 1960, cuando Ibrahim Mata<br> 
			compraron la pizzería <strong>DomiPizza's</strong>, con una inversión inicial de 950 dólares. El<br> 
			local estaba situado en Lecheria, y la idea de Ibrahim era vender<br>
			Hamburguesas a domicilio a las personas de las residencias cercanas. Aquella<br> 
			experiencia no marchaba como tenían previsto.
		</p>
		<p id="para">
			A pesar de todo, Ibrahim se mantuvo al frente del restaurante y tomó<br>
			decisiones importantes para su futuro, como reducir la carta de productos y<br> 
			establecer un reparto a domicilio gratuito. Después de adquirir dos<br> 
			restaurantes más en Barcelona, en 1965 renombró sus tres locales como<br> 
			<strong>Ibra's Burger Grill</strong>.
		</p>
		</div>
		<table class="nostrosimgs">
			<tr>
					<td>
					    <a href="menu">
						<button>
							VER EL MENÚ HOY 
						</button>
						</a>
						<a href="menu">
						<button id='greenbutton'>
							PEDIR AHORA
						</button>
						</a>
					</td>
				</tr>
		</table>
	</div>
	<div class="nostrosclient">
		<div class="bgcolor">
			<img src="<?php echo asset('images/Burguer.png')?>" alt="burger-icon" width="50" height=50 align="top center">
			<br>
			<h2 class="intro">Lo que dicen los clientes</h2>
			<table align="center" width=80%; >
				<tr >
					<td id='tabledata'>
						<p id="largefont">Que Hamburguesa mas Increible</p>
						<p id='whitecolor'>Voy con mi familia a compertir de la buena comida y servicio que prestan, los recomiendo a mis amigos, los felicito</p>
						<p id='whitecolor'><span id='redtext'>Daiane Smith</span>,Cliente</p>
					</td>
					<td id='tabledata'>
						<p id="largefont">La mejor hamburguesa de la ciudad</p>
						<p id='whitecolor'>Soy amante de la buena hamburguesa y puedo decir que me gusta lo bien que la preparan, el hambiente es como y muy familiar, cada ves que tengo la oportunidad de darme un gusto lo visito porque se que sere bien atendido y comere una excelente hamburguesa</p>
						<p id='whitecolor'><span id='redtext'>Michael Williams</span>,Cliente</p>
					</td>
					<td id='tabledata'>
						<p id="largefont">Nos encanta alli</p>
						<p id='whitecolor'>El lugar es como y muy agradable para compartir y disfrutar de una buena comida, el servicio es de primera al igual de sus instalaciones, los recomiendo como el mejor sitio para comer</p>
						<p id='whitecolor'><span id='redtext'>Shawn Gaines</span>,Cliente</p>
					</td>
				</tr>
			</table>
		</div>
	</div>
	
	<footer>
		<div class="bgcolor">
			<img src="<?php echo asset('images/5.png')?>" class="logoFooter" align="center">
			<p>	
				<span id="title">Habla a:</span><br>
				Av. Intercomunal, sectro la Mora, calle 8
			</p>
			<p>
				<span id="title">Telefono:</span><br>
				+58 251 261 00 01
			</p>
			<p>
				<span id="title">Correo:</span><br>
				yourmail@gmail.com
			</p>
			<p>
				<a href="#" class="fa fa-pinterest"></a>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-dribbble"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-vimeo"></a>
			</p>
			<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo asset('js/main.js')?>"></script>
</body>
</html>