@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<title>Inicio</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
</head>
<body id="wrapper" class="rest">

	<div id ="modal" class="modal-register-gradeout">
		   
		<div id ="modal-register">
			<span id = "closebtn" class="closebtn">&times;</span>
			<div id ="register-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Registro de Usario
			</div>
			<br><br>
			<hr>


			<form action="register" method="post" id="registration-form">
				{{ csrf_field() }}

				<p style="color: red;"> 

				@error('username') {{ $message }} @enderror 
				 @error('email') {{ $message }} @enderror
				 @error('email') {{ $message }} @enderror 
				 @error('password') {{ $message }} @enderror
				 @error('repeatpass') {{ $message }} @enderror
				  @error('address') {{ $message }} @enderror


				</p>

				<label for="fullname">Nombre y apellido:</label>
				<input type="text" name="username" id=fullname required
				    title="Username must have only alphabets and numbers."
				    pattern="^[a-zA-Z0-9]*$"
				    >

				<label for="mail">Correo:</label>
				<input type="email" name="email" id="mail" required
				    title="Example email: youremail@gmail.com"
				    pattern="[a-z0-9._%+-]+@gmail.com">
 

				<label for="pass">Contrasena:</label>
				<input type="password" name="password" id="pass" required
				    title="Password must contain at least 8 characters upto 10 characters, including atleast one uppercase, lowercase, number and special character." 
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >

				<label for="repeatPass">Repetir Contrasena:</label>
				<input type="password" name="repeatpass" id="repeatPass" required
				    title="Please enter the same Password as before."
				    pattern='^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$' 
				    >
				<label for="address">Direccion:</label>
				<textarea name="address" id = "addresss" required></textarea>

				<input type="submit" name="submitregistration" value="Cargar" id  ="sendBtn">

			</form>
			
		</div>
	</div>

	<div id ="modal1" class="modal-login-gradeout">

		<div id ="modal-login">
			<span id = "closebtn1" class="closebtn">&times;</span>
			<div id ="login-title">				
				<img id="burger-icon" src="<?php echo asset('images/Burguer.png')?>">
				Iniciar Session
			</div>
			<br><br>
			<hr>


			<form action="login" method="post" id="login-form">
				{{ csrf_field() }}

				<p style="color: red;">
					@error('username') {{ $message }} @enderror
					@error('password') {{ $message }} @enderror 


				</p>

				<label for="userName">Usuario:</label>
				<input type="text" name="username" id=userName required>

				<label for="pass1">Contrasena:</label>
				<input type="password" name="password" id="pass1" required >

				<a href="#"><input type="submit" value="Entrar" id  ="enterBtn"></a>

				@if(Session::has('message'))
              	<p>
        	    	{{ Session::get('message') }}
               	</p>
           		@endif 

			</form>
		</div>
	</div>


	<header class="rest">
		<div id = "header-gradeout">
			<img src="images/5.png" class="logo" width="100px" align="center" />
				@if (Session::has('user'))
				{
					<a class="active" href="index.php">INICIO</a>
					<a href="sobrenostros">SOBRE NOSTROS</a>
					<a href="menu">MENU</a>
					<a href="blog/">BLOG</a>
					<a  href="contacto">CONTACTO</a>
					<a  href="editarperfil">EDITAR PERFIL</a>
					<a  href="logout">CERRAR SESION</a> 
				}
				@else{
					<a class="active" href="index.php">INICIO</a>
					<a href="sobrenostros">SOBRE NOSTROS</a>
					<a href="menu">MENU</a>
					<a href="blog/">BLOG</a>
					<a  href="contacto">CONTACTO</a>
					<a id ="registerBtn" >REGISTRO</a>
					<a id ="loginBtn" >INICIAR SESION</a>
				}
			    @endif
			
		</div>
		<div class="iniciobanner">
			
		<div id ="banner-gradeout">
				<div class ="banner-text">
					<h3 class="intro">LAS MEJORES DE LA CIUDAD</h3>
					<h1 class='intro'>Hamburguesas</h1>
					<a href="menu"><button>VER MENÚ HOY</button></a>
				</div>
			</div>
		</div>	

	</header>
	
	<div class="picturesForHistory" id="border">
		 <img src="<?php echo asset('images/Burguer.png')?>" alt="burger-icon" width="50" height="50" align="top center"> 
		<br>
		<h2 class="intro">Nuestra historia</h2>
			<p id="para">
				Los orígenes se remontan al 10 de junio de 1960, cuando Ibrahim Mata<br> 
				compraron la Hamburgueseria “Soy Yo” con una  inversión inicial de 950 dólares. El<br> 
				local estaba situado en Lecheria, y la idea de Ibrahim era vender<br>
				Hamburguesas a domicilio a las personas de las residencias cercanas. Aquella<br>
				experiencia no marchaba como tenían previsto.
			</p>
			<p id="para2" class = "second-para">
				A pesar de todo, Ibrahim se mantuvo al frente del restaurante y tomó<br> 
				decisiones importantes para su futuro, como reducir la carta de productos y<br> 
				establecer un reparto a domicilio gratuito. Después de adquirir dos<br> 
				restaurantes más en Barcelona, en 1965 renombró sus tres locales como<br> 
				<strong>Ibra's Burger Grill</strong>.
			</p>
		
	</div>
	
	<!-- echo $burgers; -->
    @foreach($burgers as $b){
    	@if( $b->burger_name  == "mixta")
    		<?php $mname = $b->burger_name ?>
    		<?php $mimage = $b->image ?>
    		<?php $mprice = $b->price ?>
		@elseif( $b->burger_name  == "pollo")
			<?php $pname = $b->burger_name ?>
    		<?php $pimage = $b->image ?>
    		<?php $pprice = $b->price ?>
    	@elseif( $b->burger_name  == "carne")
			<?php $cname = $b->burger_name ?>
    		<?php $cimage = $b->image ?>
    		<?php $cprice = $b->price ?>
    	@endif
	}
	@endforeach
    	
	<div class="bestsellers">
		<div class="bgcolor">
			<img src="<?php echo asset('images/Burguer.png')?>" alt="burger-icon" width="50" height=50 align="top center">
			<br>
			<h2 class="intro">Las más vendidos</h2>
			<table align="center" >
				<tr >
					<td>
						<div class="product">			
							<img src="{{ $mimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $mname }}</span><br>	
								<span>{{ $mprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $pimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $pname }}</span><br>	
								<span>{{ $pprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $cimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $cname }}</span><br>	
								<span>{{ $cprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $mimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $mname }}</span><br>	
								<span>{{ $mprice }}</span>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="product">			
							<img src="{{ $pimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $pname }}</span><br>	
								<span>{{ $pprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $cimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $cname }}</span><br>	
								<span>{{ $cprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $mimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $mname }}</span><br>	
								<span>{{ $mprice }}</span>
							</div>
						</div>
					</td>
					<td>
						<div class="product">			
							<img src="{{ $pimage }}" alt="mixta" width="180px" height="180px">
							<div class="desc">
								<span>{{ $pname }}</span><br>	
								<span>{{ $pprice }}</span>
							</div>
						</div>
					</td>
				</tr>
			</table>
				
			<a href="menu"><button id='vermenuhoy'>VER MENÚ HOY</button></a>
		</div>
	</div>
	<footer>
		<div class="bgcolor">
			<img src="<?php echo asset('images/5.png')?>" class="logoFooter" align="center">
			<p>	
				<span id="title">Habla a:</span><br>
				Av. Intercomunal, sectro la Mora, calle 8
			</p>
			<p>
				<span id="title">Telefono:</span><br>
				+58 251 261 00 01
			</p>
			<p>
				<span id="title">Correo:</span><br>
				yourmail@gmail.com
			</p>
			<p>
				<a href="#" class="fa fa-pinterest"></a>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-dribbble"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-vimeo"></a>
			</p>
			<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo asset('js/main.js')?>"></script>
</body>
</html>