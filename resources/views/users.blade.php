<!DOCTYPE html>
<html>
<head>
	<title>Admin Page</title>
	<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/ibras.css')?>">
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>
<body id="wrapper" class ="rest">
	<header class ="rest">
		<div id = "header-gradeout">
			<img src="images/5.png" class="logo" width="100px" align="center" />
				<a class="active">MANAGE USERS</a>
				<a href ="products">MANAGE PRODUCTS</a>
				<a href="logout" >CERRAR SESION</a>
		</div>

	</header>

	<div id ="userctn">
		<h1>User List</h1>
		<form method="post" action="/update_users">
		    
		@method('PATCH')

		<table class ="user-list">
			<thead class = "user-table-head">
				<tr>

					<th>User id</th>
					<th>Username</th>
					<th>Email</th>
					<th>Mobile</th>
					<th>Admin</th>
				</tr>
			</thead>
			<tbody class="user-table-body">
                @csrf
                
                

                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->username}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->mobile}}</td>
                    <td>
                    <input type='checkbox' name='Adminuser[]' value ='{{$user->id}}' <?php 
                    if ($user->role == "admin")
                        echo "checked";
                    ?>>
                    </td>
                </tr>

                @endforeach

			
    			
				
			</tbody>


		</table>
	</div>
    
	<input id = "submitbtn" type="submit" name="submitrecord" placeholder="Update records">
		
	
	</form>
	
	

<footer style="background:none;margin-top: 8rem;">
		<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
	</footer>
</body>
</html> 