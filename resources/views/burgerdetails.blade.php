<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>BurgerDetails</title>
		<link href="//db.onlinewebfonts.com/c/41f5e8ff1d98d490a19c6d48ea7b74b1?family=Beyond+The+Mountains" rel="stylesheet" type="text/css"/>
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
    	<link rel="stylesheet" type="text/css" href="/css/ibras.css">
	</head>
	<body id="wrapper" class="rest">
        <header class="rest" id="burgerheader">
        <img src="/images/5.png" class="logo" width="100px" align="center" />
            <a href="/menu">MENU</a>
            
            <a href="/cart">CART		
			</a>
            <div class="burgerbanner">
            <div class ="banner-text">
					<h1 class="intro">Online Order System</h1>
				</div>
                
            </div>
        </header>
        <main>
        	<div class="burger-wrapper burgernamecolor">
		    <table>
		        <thead class="burgernamecolor" size="100px;"> 
		           <h2>{{$burger->description}}</h2> 
		        </thead>
		        <tr>
		            <td>
		                <img src="/{{$burger->image}}" width="400" height="400" alt="{{$burger->burger_name}}">
		            </td>
		            <td>
		                <h1 class="burgernamecolor">{{$burger->burger_name}}</h1>
		                <div>
		                    <span>Price:</span>
		                    <span class="price">
		                    &dollar;{{$burger->price}}
		                </span>
		                </div>
		                
		                <form action="/addtocart" method="post">
		                	@csrf
		                    <div>
		                        <label>Quantity:</label>
		                    <input type="number" name="quantity" value="1" min="1" max="25" placeholder="Quantity" required style="width: 50px;">
		                    </div>
		                    <br>
		                    <input type="hidden" name="id" value="{{$burger->id}}">
		                    <div>
		                        <input type="submit" name ="submittocart" value="Add To Cart">
		                    </div>
		                    @if(Session::has('success'))
		              	<div class="alert alert-success">
		        	    	{{ Session::get('success') }}
		               	</div>
           			@endif 
		                </form>
		            </td>
		        </tr>
		        
		    </table>
		    
		</div>
        </main>
        <footer id="burgerfooter">
		<div class="bgcolor">
			<img src="/images/5.png" class="logoFooter" align="center">
			<p>	
				<span id="title">Habla a:</span><br>
				Av. Intercomunal, sectro la Mora, calle 8
			</p>
			<p>
				<span id="title">Telefono:</span><br>
				+58 251 261 00 01
			</p>
			<p>
				<span id="title">Correo:</span><br>
				yourmail@gmail.com
			</p>
			<p>
				<a href="#" class="fa fa-pinterest"></a>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-dribbble"></a>
				<a href="#" class="fa fa-google"></a>
				<a href="#" class="fa fa-linkedin"></a>
				<a href="#" class="fa fa-vimeo"></a>
			</p>
			<p>
				Copyright  &copy;2020 Todos los derechos reservados | Este sitio esta hecho con &hearts; por DiazApps
			</p>
		</div>
	</footer>
        // <script src="/js/script.js"></script>
    </body>
</html>