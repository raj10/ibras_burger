<?php

namespace App\Http\Controllers;

use Request;
use File;

class BurgerController extends Controller
{
    function show(){
    	$burgers = \App\Burger::all();



    	return view('products', compact('burgers'));
    }

    function store(){

    	$newburger = request()->validate([

    		'bname' => 'required'

    	]);


    	$burger = new \App\Burger();

    	$burger->burger_name = request('bname');
    	$burger->description = request('bdesc');
    	$burger->price = request('bprice');

    	if(Request::hasFile('imageToUpload')){

			$file = Request::file('imageToUpload');
			$file->move('images', $file->getClientOriginalName());
			$burger->image= "images/".$file->getClientOriginalName();
		}


    	$burger->save();

    	return redirect()->back();

    }


    function show_burger(\App\Burger $product){

    	return view('editburger', compact('product'));

    }


    function update (\App\Burger $burger) {
    	$newburger = request()->validate([

    		'bname' => 'required'

    	]);


    	$burger->burger_name = request('bname');
    	$burger->description = request('bdesc');
    	$burger->price = request('bprice');

    	if(Request::hasFile('imageToUpload')){

    		$image_path = $burger->image; 	
			if (file_exists($image_path)){
   			 File::delete($image_path);
			}

			$file = Request::file('imageToUpload');
			$file->move('images', $file->getClientOriginalName());
			$burger->image= "images/".$file->getClientOriginalName();
		}


    	$burger->update();

    	return redirect()->back();


    }

    function destroy(\App\Burger $product){

    	$image_path = $product->image; 	
		if (file_exists($image_path)){
   			 File::delete($image_path);
		}

    	$product->delete();

    	return redirect('/products');

    }

    function displayburgerdetails(\App\Burger $burger) {
        
        return view('burgerdetails', compact('burger'));
    }

    function addtocart(){
        $burger = \App\Burger::find(request('id'));
        $id = $burger->id;
        // dd($id);
        $cart = session()->get('cart');

        // $price = request('price');
        $quantity = request('quantity');

        if(!$cart) {
 
            $cart = [
                    $id => [
                        "name" => $burger->burger_name,
                        "id"  => $id,
                        "quantity" => $quantity,
                        "price" => $burger->price,
                        "photo" => $burger->image
                    ]
            ];
 
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;
 
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        } else {
            $cart[$id] = [
                "name" => $burger->burger_name,
                "id"  => $id,
                "quantity" => $quantity,
                "price" => $burger->price,
                "photo" => $burger->image
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        return view('cart', compact('burgers'));
    }

    function displaycart(){
        $burgers = session()->get('cart');
        // dd($burgers);
        return view('cart', compact('burgers'));
    }
}
