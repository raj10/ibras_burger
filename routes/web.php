<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/index', 'IndexController@index');

Route::get('/menu', 'MenuController@post');

Route::get('/sobrenostros', function() {
	return view('sobrenostros');
});

Route::get('/contacto', [
	'uses'=>'ContactController@getContact',
	'as'=>'contacto']);
Route::post('contacto', [
	'uses'=>'ContactController@saveContact',
	'as'=>'contacto']);

Route::post('register', [
	'uses'=>'RegistrationController@store',
	'as'=>'register']);

Route::get('/register/{activationcode}', 'RegistrationController@update');

Route::post('login', 'LoginController@authenticate');
Route::get('logout', 'LoginController@destroy');

Route::get('editarperfil', 'EditarperfilController@display');
Route::patch('editarperfil/profile', 'EditarperfilController@updateprofile');
Route::patch('editarperfil/password', 'EditarperfilController@updatepassword');



//User routes
Route::get('/users', 'UserController@show');
Route::patch('/update_users', 'UserController@update_users');

Route::get('/burgerdetails/{burger}', 'BurgerController@displayburgerdetails');
Route::get('/cart', 'BurgerController@displaycart');
Route::post('/addtocart', 'BurgerController@addtocart');
Route::get('/remove/{burger}', 'BurgerController@removefromcart');
Route::post('/updatecart', 'BurgerController@updatecart');

//Burger routes
Route::get('/products', 'BurgerController@show');
Route::post('/burger/create', 'BurgerController@store');

Route::get('/products/{product}', 'BurgerController@show_burger');


Route::patch('/products/{burger}/update', 'BurgerController@update');

Route::delete('/products/{product}/delete', 'BurgerController@destroy');
