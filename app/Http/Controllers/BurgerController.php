<?php

namespace App\Http\Controllers;

use Request;
use File;

class BurgerController extends Controller
{
    function show(){
        $burgers = \App\Burger::all();

        return view('products', compact('burgers'));
    }

    function store(){

        $newburger = request()->validate([

            'bname' => 'required'

        ]);


        $burger = new \App\Burger();

        $burger->burger_name = request('bname');
        $burger->description = request('bdesc');
        $burger->price = request('bprice');

        if(Request::hasFile('imageToUpload')){

            $file = Request::file('imageToUpload');
            $file->move('images', $file->getClientOriginalName());
            $burger->image= "images/".$file->getClientOriginalName();
        }


        $burger->save();

        return redirect()->back();

    }


    function show_burger(\App\Burger $product){

        return view('editburger', compact('product'));

    }


    function update (\App\Burger $burger) {
        $newburger = request()->validate([

            'bname' => 'required'

        ]);


        $burger->burger_name = request('bname');
        $burger->description = request('bdesc');
        $burger->price = request('bprice');

        if(Request::hasFile('imageToUpload')){

            $image_path = $burger->image;   
            if (file_exists($image_path)){
             File::delete($image_path);
            }

            $file = Request::file('imageToUpload');
            $file->move('images', $file->getClientOriginalName());
            $burger->image= "images/".$file->getClientOriginalName();
        }


        $burger->update();

        return redirect()->back();


    }

    function destroy(\App\Burger $product){

        $image_path = $product->image;  
        if (file_exists($image_path)){
             File::delete($image_path);
        }

        $product->delete();

        return redirect('/products');

    }

    function displayburgerdetails(\App\Burger $burger) {
        
        return view('burgerdetails', compact('burger'));
    }

    function addtocart(){
        $burger = \App\Burger::find(request('id'));
        $id = $burger->id;
        
        $cart = session()->get('cart');

        $quantity = request('quantity');

        if(!$cart) {
 
            $cart = [
                    $id => [
                        "name" => $burger->burger_name,
                        "id"  => $id,
                        "quantity" => $quantity,
                        "price" => $burger->price,
                        "photo" => $burger->image
                    ]
            ];
 
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;
 
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
 
        } else {
            $cart[$id] = [
                "name" => $burger->burger_name,
                "id"  => $id,
                "quantity" => $quantity,
                "price" => $burger->price,
                "photo" => $burger->image
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        return view('cart', compact('burgers'));
    }

    function displaycart(){
        $burgers = session()->get('cart');
    
        return view('cart', compact('burgers'));
    }

    function removefromcart(\App\Burger $burger){
        $burgers = session()->get('cart');
        unset($burgers[$burger->id]);
        session()->put('cart', $burgers);
        return redirect()->back();
    }

    function updatecart(){
        // print_r(null!==(session('cart')));
        $burgers = session()->get('cart');
        // print_r($burgers);
        $data = request()->all();
        $keys = array_keys( $data );
        print_r(array_key_last($data));
        if($data[array_key_last($data)] == "Update"){
            foreach($data as $key => $value){

            printf("%s : %s",$key,$value);
        //     // printf("\n");
            if (strpos($key, 'quantity') !== false && is_numeric($value)) {
                
                $id = str_replace('quantity-', '', $key);
                print($id);
                $quantity = (int)$value;
                // Always do checks and validation
                if (is_numeric($id) && (null!==(session('cart'))) && $quantity > 0) {
                    // Update new quantity
                    // print_r($burgers[$id]['quantity']);
                    print_r($quantity);
                    $burgers[$id]['quantity'] = $quantity;
                }
            }
        }
            // print_r($burgers);
        session()->put('cart', $burgers);
        return redirect()->back();
        }
        else if ($data[array_key_last($data)] == "PlaceOrder") {
            session()->forget('cart'); 
            return view('placeorder');
        }
        
    }
}
