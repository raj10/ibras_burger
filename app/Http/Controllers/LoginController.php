<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
	public function authenticate(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            
            $user = Auth::user();
            if ($user->email_verified==1) {
                Session::put('user', $request->username);
                Session::put('id', $user->id);
                if($user->role == 'user') {
                    return redirect()->intended('menu');
                } else if($user->role == 'admin'){
                    return redirect()->intended('users');
                }
            } elseif($user->email_verified==0){
                session()->flash('message', 'Please activate your email and try again.');
                return redirect()->back();
            }
            
            
        } else {
            session()->flash('message', 'Please check your username and password again.');
            return redirect()->back();
        }
    }

    public function updateProfile(){

    }

    public function destroy()
    {
        Auth::logout();
        Session::flush();
        return redirect()->to('/');
    }
}
