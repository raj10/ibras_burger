<?php
namespace App\Http\Controllers;

use App\User;
use App\Mail\VerifyMail;
use Illuminate\Http\Request;
use DB;

class RegistrationController extends Controller
{

     public function store()
    {
        

        $user_array= request()->validate( [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required| min:6',
            'address' => 'required'
        ]);
        $activationcode = uniqid();
                     
        $user_array['activationcode'] = $activationcode;
        $user_array['email_verified'] = 0;


        $user = User::create($user_array);

        \Mail::to($user->email)->send(new VerifyMail($user));

        session()->flash('message', 'Please check your email to activate your account');
        
        return redirect()->to('/index');
    }

    public function update($code)
    {
       // print("Your email has been verified successfully."."<a href='/index.php'>Click here to visit Homepage</a>");

        $user = DB::table('users')->where('activationcode', $code)->first();
        

        $user->email_verified = 1;

        $user = User::where('activationcode', $code)->update([
            "email_verified" => "1"
        ]);

        print("Your email has been verified successfully." . " <a href='/menu'>Click here to visit menu</a>");

    }


}
