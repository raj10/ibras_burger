<?php

namespace App\Http\Controllers;
use App\Contact;
use Illuminate\Http\Request;
use Mail; 

class ContactController extends Controller
{
    public function getContact(){
		return view('contacto');
    }

    public function saveContact(Request $request){

    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email',
    		'subject' => 'required|min:4|max:40',
    		'message' => 'required|min:10|max:300'
    	]);

    	$contact = new Contact;

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;

        $contact->save();
        
        return back()->with('success', 'Thank you for contacting us! We\'ll look into it!');

    }
}
