<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function VerifyEmail($token = null)
    {
    	if($token == null) {

    		session()->flash('message', 'Invalid Login attempt');

    		return redirect()->route('/');

    	}


       if($user == null ){

       	session()->flash('message', 'Invalid Login attempt');

        return redirect()->route('/');

       }

       $user->update([

        'email_verified_at' => Carbon::now()

       ]);
       
       	session()->flash('message', 'Your account is activated, you can log in now');

        return redirect()->route('/');

    }

}