<?php

namespace App\Http\Controllers;

Use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
// use Request;

class EditarperfilController extends Controller
{
    public function display(){
    	$id = Session::get('user');
    	$user = DB::table('users')->where('username', $id)->first();

    	$user = \App\User::find($user->id);
    	

		// echo $user;
		return view('editarprofil', compact('user'));
    }


    public function updateprofile(Request $request){
    	$id = Session::get('user');
    	$user = DB::table('users')->where('username', $id)->first();


        $this->validate($request, [
            'firstname' => 'required|min:1|max:40',
            'lastname' => 'required|min:1|max:40',
            'email' => 'required|email'
        ]);

    	$user = \App\User::find($user->id);

    	$user->firstname = request('firstname');
    	$user->lastname = request('lastname');
    	$user->country = request('country');
    	$user->mobile = request('mobile');

    	$user->update();
    	return redirect()->back()->with('successProfile', 'Your profile details has been updated successfully!');
    }

    public function updatepassword(){
    	$id = Session::get('user');
    	$user = DB::table('users')->where('username', $id)->first();

    	$user = \App\User::find($user->id);

    	$user->password = request('password');

    	$user->update();
    	return redirect()->back()->with('success', 'Your password has been updated successfully!');
    }
}
