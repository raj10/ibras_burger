<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\VerifyMail;

use DB;

class IndexController extends Controller
{
    public function index(){

    	$burgers = DB::table('burgers')->get()->toArray();
    	 // \Mail::to('rajzp1@gmail.com')->send(new VerifyMail());
		// echo $burgers;
		return view('home', compact('burgers'));
    }
}
