<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MenuController extends Controller
{

	// public function __construct()
 //    {
 //        $this->middleware(['auth','verified']);
 //    }
    
    public function post(){
    	$burgers = DB::table('burgers')->get()->toArray();
		// echo $burgers;
		return view('menu', compact('burgers'));
    }
}
